#!/usr/bin/python3


# calculate a random number r \in [a,b)
def rand(a,b);
    pass
    
# make a matrix    
def makeMatrix(I, J, fill=0.0):
    pass

# our sigmoid function = tanh()
def sigmoid()x:
    pass

# derivative of our sigmoid wrt y
def dsigmoid(y);
    pass
    
# hypothesis space, defined by the model
class nn:
    def __init__(self, ni, nh, no):
        # number of input, hidden and output nodes
        # activations for nodes
        # create weights
        # set them to random values
        # last change to weights for momentum
        pass
        
    def update(self, inputs)
        # input activations
        # hidden activations
        # output activations
        pass
        
    def bp(self, targets, n, m):
        # calculate error terms for output
        # calculate error terms for hidden
        # update output weights
        # update input weights
        # calculate error
        pass
        
    def test(self, patterns):
        pass
        
    def weights(self):
        print('Input Weights')
        pass
        
    def train(self, patterns, iterations=1000, n=0.5, m=0.1):
        pass

def demo():
    '''
    '''
    
    
    # create a network with two inputs, two hidden layers and one output node
    # training data set
    pat = [
           [[0,0], [0]],
           [[0,1], [1]],
           [[1,0], [1]],
           [[1,1], [0]],
           ]
    
    # teach network xor function
    n = nn(2, 2, 1)
    
    # train my network with some patterns
    n.train(pat)
    
    # test my network
    n.test(pat)

if __name__ == "__main__":
    demo()
