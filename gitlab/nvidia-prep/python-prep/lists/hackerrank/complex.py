#/usr/bin/python3

'''
Classes: Dealing with Complex Numbers

I am given 2 complex numbers
 
I have to print the result of their

+ addition operation
- subtraction operation
* multiplication operation
/ division operation
% modulus operation


specifications

input format
- one line of input
- the real and imaginary part of a number separated by a space

output format
- for 2 complex numbers C and D
- the output should be in the following format
- sequence on separate lines

C + D
C - D
C * D
C / D
mod(C)
mod(D)

For complex numbers with non-zero real(A)
and complex part(B) 
the output should be in the following format
A + Bi

Replace the plus symbol (+)
with a minus symbol (-) when B < 0



'''

import math

if __name__ == '__main__':

    


