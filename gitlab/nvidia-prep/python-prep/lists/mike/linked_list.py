'''
TODO: Description here
'''

class SinglyLinkedList(object):
    '''
    TODO: Class description
    '''

    def __init__(self):
        '''
        TODO: description
        '''
        pass

    def __len__(self):
        '''
        TODO: description
        '''
        pass

    def insert(self, value, idx):
        '''
        TODO: description
        '''
        pass

    def remove(self, idx):
        '''
        TODO: description
        '''
        pass

    def append(self, value):
        pass

    def prepend(self, value):
        pass

    def push(self, value):
        self.prepend(value)

    def pop(self):
        pass

    def extend(self, iterable):
        pass

    def __add__(a, b):
        '''
        Return a new linked-list with contents from a then contents from b.
        This is called when you do "a + b".
        '''
        pass

    def __contains__(self, value):
        '''
        Return true if value is in this container.  False otherwise.
        This is called if you say "value in self"
        '''
        pass

    def __delitem__(self, idx):
        '''
        Remove the item at index idx.
        This is called if you say "del linkedlist[i]" (I think)

        Note: if called like so: "del linkedlist[i:j]", then idx is a slice
        object with attributes "start", "stop", and "step".
        '''
        pass

    def __getitem__(self, idx):
        '''
        Return item at index idx.
        This is called if you say "linkedlist[i]"

        Note: if called like so: "linkedlist[i:j]", then idx is a slice object
        with attributes "start", "stop", and "step".
        '''
        pass

    def __setitem__(self, idx, value):
        '''
        Replace the value at index idx to value.
        This is called if you say "linkedlist[idx] = value"

        Note: if called like so: "linkedlist[i:j] = sequence", then idx is a
        slice object with attributes "start", "stop", and "step".
        '''
        pass

    def __eq__(self, other):
        '''
        Return True if the other linked list is the same as this one.
        This is called if you say "a == b"
        '''
        pass

    def __ne__(self, other):
        '''
        Return True if the other linked list is the same as this one.
        This is called if you say "a != b"
        '''
        pass

