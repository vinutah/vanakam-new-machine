'''
Data structure 
- Implementation 
- from scratch
- Singly Linked List 
'''

'''
TODO add size as a member
TODO think about tail 
TODO prev and current abstraction
'''


class Node(object):
    
    def __init__(self, value=None):
        '''
        >>> a = Node()
        >>> a = Node(a)
        '''
        self.data = value
        self.next = None

class SinglyLinkedList(object):
    
    def __init__(self,value=None):
        '''
        >>> l = SinglyLinkedList()
        >>> type(l)
        <class 'll.SinglyLinkedList'>
        '''
        self.head = None
        
       
    def __str__(self):
        '''
        >>> l = SinglyLinkedList()
        >>> print(l)
        []
        
        >>> l.head = Node('a')
        >>> l.head.next = None
        >>> print(l)
        [a]
        
        >>> l2 = Node('b')
        >>> l.head.next = l2
        >>> l2.next = None
        >>> print(l)
        [a->b]
        
        >>> l3 = Node('c')
        >>> l2.next = l3
        >>> l3.next = None
        >>> print(l)
        [a->b->c]
        '''
        x = self.head
        
        output = '['
        
        while (x is not None):
            delim = '->' if x.next is not None else ''
            output += str(x.data) + delim
            x = x.next
            
        output += ']'
        
        return output
   
    def __len__(self):
        '''
        >>> l = SinglyLinkedList()
        >>> print(len(l))
        0
        >>> l.head = Node('a')
        >>> print(len(l)) 
        1
        >>> l.head.next = Node('b')
        >>> print(len(l))
        2
        >>> l.head.next.next = Node('c')
        >>> print(len(l))
        3
        '''
        x = self.head
        length = 0
        while (x is not None):
            x = x.next
            length += 1
        return length
        
    def insert(self, value, idx):
        '''
        >>> l = SinglyLinkedList()
        >>> l.insert('b',0)
        >>> print(l)
        [b]
        
        >>> l.insert('d',1)
        >>> print(l)
        [b->d]
        
        >>> l.insert('c',1)
        >>> print(l)
        [b->c->d]
        
        >>> l.insert('a',0)
        >>> print(l)
        [a->b->c->d]
        
        >>> l.insert('e',5)
        Traceback (most recent call last):
        ...
        IndexError: linked list index out of range
        
        >>> l.insert('f',-1)
        Traceback (most recent call last):
        ...
        IndexError: linked list index out of range
        '''
        if ( idx < 0 or idx > len(self) ):
            raise IndexError('linked list index out of range')
        
        x = self.head
        if idx == 0:
            self.head      = Node(value)
            self.head.next = x
        else:
            for _ in range(idx-1):
                x = x.next
            right_end = x.next
            x.next = Node(value)
            x.next.next = right_end
            
        def remove(self, idx):
            '''
            >>> l = SinglyLinkedList()
            >>> l.insert('a',0)
            >>> l.insert('b',1)
            >>> l.insert('c',2)
            >>> l.insert('d',3)
            >>> l.remove()
            '''
            pass
