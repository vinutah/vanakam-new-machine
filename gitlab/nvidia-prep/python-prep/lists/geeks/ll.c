/*
 * Representation in c
 * 
 * a linked list is represented by a pointer to the first node
 * of the linked list
 * 
 * the first node is called head
 * 
 * if the linked list is empty, then the value of the head is NULL
 * each node in a list consists of atleast two parts
 * data and pointer
 * 
 * In c we can represent a node using structures
 * example of a linked list using structures
 * this linked list can hold integer data
 * /
