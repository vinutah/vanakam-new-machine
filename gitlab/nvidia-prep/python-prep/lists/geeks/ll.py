"""
linked list 
    linear data struture
    elements are not stored at contigious locations
    the elements are linked using pointers
    
why
    arrays can be used to store linear data of similar types
    issues
    - the size of the arrays is fixed
    - need to know the upper limit
    on the number of elements in advance
    - allocated memory is equal to the upper limit irrespective of the usage
    - deletion is expenisve

pros
    - dynamic size
    - ease of insertion/ deletion
    
cons
    - random access not allowed
    - extra memory space for a pointer is required with each element of the list
    
representaion in c
    - a pointer to the first node of the linked list
    - the first node is called head
    - if the linked list is empty then value of head is NULL
    - each node in a list consists of atleast two parts
    - data
    - pointer to the next node
    - represent a node using structure

"""

# Node class

class Node:
    
    # a function to initialize the node object
    def __init__(self, data):
        self.data = data
        self.next = None
        
class LinkedList:
    
    # function to initialize the linked list object
    def __init__(self):
        self.head = None
        
    # traversal , methods
    def printList(self);
    
    
if __name__ == '__main__':

    # start with empty list
    llist = LinkedList()
    
    llist.head = Node(1)
    second = Node(2)
    third = Node(3)
    
    llist.head.next = second
    second.next = third
    
    

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
