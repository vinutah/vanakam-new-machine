## Symless

## Install Google-Chrome on Ubuntu

* First download the package 64 bit

```wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb```

* Install the package, forcing install of dependencies:

```sudo dpkg -i --force-depends google-chrome-stable_current_amd64.deb```

* In the case that any dependencies did not install
  you can force them via

```sudo apt install -f```

## Anaconda
`wget https://repo.anaconda.com/archive/Anaconda3-2018.12-Linux-x86_64.sh`

## PyTorch
`conda install pytorch torchvision cudatoolkit=10.0 -c pytorch`

## Base software

```sudo apt install vim-gnome terminator git htop tree screen texlive-full okular pandoc unrar cmake```

## FONTS
```
sudo apt-get update -y
sudo apt-get install -y fonts-inconsolata
```

## EDITOR
* vim-gnome
```
scp u1001075@notchpeak2.chpc.utah.edu:/uufs/chpc.utah.edu/common/home/u1001075/sandbox/writing/configurations.tar.gz .
tar -xzvf configurations.tar.gz
ln -s configurations/vim .vim
ln -s configurations/vimrc ~/.vimrc
sudo apt install vim-gnome
```

* Atom
```
https://atom.io/download/deb
```

## GPU RELATED
```
install cuda latest
```

```
vim ~/.bashrc
export PATH=/usr/local/cuda/bin${PATH:+:${PATH}} 
export LD_LIBRARY_PATH=/usr/local/cuda/lib64\${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}
```
```
cd /usr/local/cuda/samples/5_Simulations/nbody
sudo make
./nbody 
```

## CUDNN
```
cd ~/Downloads/
tar -xzvf cudnn-9.0-linux-x64-v7.1.tgz 
sudo cp cuda/include/cudnn.h /usr/local/cuda/include
sudo cp cuda/lib64/libcudnn* /usr/local/cuda/lib64
sudo chmod a+r /usr/local/cuda/include/cudnn.h /usr/local/cuda/lib64/libcudnn*
sudo apt-get install libcupti-dev
```


## TRAINING CODE
```
git clone --recursive https://github.com/bearpaw/pytorch-classification.git
```


# New Machines

## Fast X

[CHPC SOFTWARES FAST X](https://www.chpc.utah.edu/role/user/software.php)

```scp u1001075@notchpeak2.chpc.utah.edu:/uufs/chpc.utah.edu/common/home/u1001075/sandbox/writing/configurations.tar.gz .```

## Codes

### CUDA 8

### Caffe

### CNN C++ Codes

## Bash

ssh-copy-id tim@just.some.other.server

## BASHRC

[Mac](http://blog.taylormcgann.com/tag/prompt-color/)

[Mac](https://stackoverflow.com/questions/12870928/mac-bash-git-ps1-command-not-found)


[Remove Full Path from Prompt](https://superuser.com/questions/60555/show-only-current-directory-name-not-full-path-on-bash-prompt)

Basically : ```Change the \w (lowercase) to \W (uppercase)```

```sh
if [ "$color_prompt" = yes ]; then
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\W\[\033[00m\]\$ '
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\W\$ '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \W\a\]$PS1"
    ;;
*)
    ;;
esac
```

## Wi-FI

### UGeust

Connect to this via the Wi-Fi Selections

### UConnect

If you just open a browser and attempt to vist a website.
It will take you to another site throught which you can 
download this .tar.bz2 file. Leave the browser open
and unzip + run this binary, then you get connected 
the the UConnect

```tar -vxjf Cloudpath-x64.tar.bz2```

## Ubuntu Updates

You should first run ```update```, then ```upgrade```. 
Neither of them automatically runs the other.

* ```apt-get update``` updates the list of available packages and their versions, 
  but it does not install or upgrade any packages.

* ```apt-get upgrade``` actually installs newer versions of the packages you have. 
  After updating the lists, the package manager knows about available updates for the software you have installed.


## Install DrRacket

```
https://download.racket-lang.org/
```

```
sudo add-apt-repository ppa:plt/racket
sudo apt-get update
```

## Setup secure shell

[Secure Shell Setup](https://gitlab.com/help/ssh/README)

* Add the contents  of ```cat ~/.ssh/id_rsa.pub``` to GitLab Accounts

## Virtual Machine

[Download VMWare Player](https://my.vmware.com/en/web/vmware/free#desktop_end_user_computing/vmware_workstation_player/12_0)

## Python libraries

## Install pip

```sudo apt install python-pip```

```sudo apt install pylint```

```pip install --upgrade pip```

## c++ related

### Google Tests

### Install graphviz
[Link](https://anaconda.org/anaconda/graphviz)

```conda install -c anaconda graphviz```

```pip install graphviz```

TODO : Add detailts of the side-effects

```sudo pip install graphviz```

## Code Blocks IDE

[Install Instructions Here](http://ubuntuhandbook.org/index.php/2016/05/install-codeblocks-ide-ubuntu-16-04/)

```
sudo add-apt-repository ppa:damien-moore/codeblocks-stable
sudo apt update
sudo apt upgrade
sudo apt install codeblocks
```

## IntelliJ IDEA

[Download Here](https://www.jetbrains.com/idea/download/download-thanks.html?platform=linux)
```tar -xzvf ideaIU-2017.2.3.tar.gz```

TODO : Add detailts of the side-effects

## Anaconda

[Download Here](https://www.anaconda.com/download/#download)

```wget https://repo.continuum.io/archive/Anaconda3-4.4.0-Linux-x86_64.sh```
```bash Anaconda3-4.4.0-Linux-x86_64.sh```
```conda install graphviz```


## Install CLion

[Direct Link](https://download.jetbrains.com/cpp/CLion-2017.2.2.tar.gz)

## Install PyCharm

[Click Link](https://www.jetbrains.com/pycharm/download/download-thanks.html?platform=linux>)
```tar -xzvf pycharm-professional-2017.2.3.tar.gz```


## JDK

[Download Here](http://www.oracle.com/technetwork/java/javase/downloads/index.html)

## Install GPU Drivers and TensorFlow

[Download Here](http://www.nvidia.com/object/gpu-accelerated-applications-tensorflow-installation.html)

## BASH ENV

```sh
export PATH="/home/vinu/anaconda3/bin:$PATH"

export PATH=/usr/local/cuda-8.0/bin${PATH:+:${PATH}}
export PATH=/home/vinu/ide/pycharm-2017.2.3/bin${PATH:+:${PATH}}
export PATH=/home/vinu//ide/idea-IU-172.3968.16/bin${PATH:+:${PATH}}


(first library path)
export LD_LIBRARY_PATH=/usr/local/cuda-8.0/lib64
export LD_LIBRARY_PATH=/usr/local/cuda-8.0/lib64\${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}

export PATH=/home/vinu/ide/racket/bin${PATH:+:${PATH}}

```

# Extra Curricular


### Chrome Plugins

* Stylish + Solarized Dark Everything

# Mouse and Keyboard

```sudo apt-get update && sudo apt-get install solaar-gnome3```

[Reference](http://www.omgubuntu.co.uk/2013/12/logitech-unifying-receiver-linux-solaar)

## Synergy

[Login](https://symless.com/login)

```sh
User Name = vinutah.soc@gmail.com
Password  = *********
```

Note Down your serial key

```sh

sudo dpkg -i --force-depends synergy-v1.8.8-stable-25a8cb2-Linux-x86_64.deb
sudo apt install -f

```

### Wallpapers

* Go get this from that Google Drive tar

### Shotwell

Go get this from Software Store In Ubuntu

### GNOME Plugins

[dim-desktop-70](https://extensions.gnome.org/extension/1130/dim-desktop-70/)
[drop-down-terminal](https://extensions.gnome.org/extension/442/drop-down-terminal/)

### Media Player

```sudo apt install vlc ````

## Messengers

### Slack

[Download Link](https://slack.com/downloads/instructions/ubuntu)

```wget https://downloads.slack-edge.com/linux_releases/slack-desktop-2.7.1-amd64.deb```

```sudo dpkg -i --force-depends slack-desktop-2.7.1-amd64.deb```

```sudo apt install -f```


### Skype

## ScreenSaver

Configure screensaver in Ubuntu

```sh
sudo apt-get remove gnome-screensaver
sudo apt-get install xscreensaver xscreensaver-data-extra xscreensaver-gl-extra
```
[Join Startup](https://askubuntu.com/questions/292995/configure-screensaver-in-ubuntu)

[Removed](https://www.howtoinstall.co/en/ubuntu/xenial/xscreensaver?action=remove)
